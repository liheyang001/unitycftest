# README #

## How to play ##
Click left mouse button to start, move your mouse to control the paddle and try to catch the ball
Once ball hits either wall, bricks or paddle, ball will bounce
You will have 3 lifes at the start, everytime ball drops, you will lose one life
You lose the game when you have no life left
There are 2 levels at the moment
Level 1: You need clear all bricks except undestructable one to go to the next level
Level 2: To win the game, you will need to hit the key and bring the key back to paddle, and after that you need to collide with the door (you don't need to clear all bricks)

## Things will be done ##
Level system - after you pass the level, you can select it from main menu
Cutscene
Ball animation
Sound effect
Mobile version

## Technique has been implemented ##
Superclass – inheritance
Random blocks & reusable code – easy to maintain
Oncolliderenter2d
Distance
Raycast2d
UI - key

## Features implemented ##
Random blocks
Gameobject follow another gameobject
Restart level
Pause game
Item - key
Booster speed

## Developed by Heyang Li, Cerebralfix ##