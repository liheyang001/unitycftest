﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : MonoBehaviour
{

    public static bool followBall;
    public GameObject ball;

    //key collide with paddle
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Destroy(gameObject);
            GameController.playerHasKey = true;
        }
    }

    private void Start()
    {
        ball = GameObject.FindGameObjectWithTag("Ball");
    }

    private void FixedUpdate()
    {
        //key follows ball if true
        if (followBall)
        {
            transform.position = ball.transform.position + new Vector3(1f,-0.5f,0);
        }
    }
}
