﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class StandardBricks : MonoBehaviour
{
    [SerializeField] private int hitsToDestroy;
    public int numberOfHits;
    private void Awake()
    {
        numberOfHits = 0;
    }

    //hit X times and destory the bricks
    private void FixedUpdate()
    {
        if(numberOfHits >= hitsToDestroy)
        {
            Destroy(this.gameObject, 0.1f);
        }
    }
}