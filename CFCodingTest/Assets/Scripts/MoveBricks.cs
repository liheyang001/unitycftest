﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBricks : StandardBricks
{
    private Rigidbody2D myRB;
    [SerializeField] private int brickMoveSpeed;
    void Awake()
    {
        myRB = GetComponent<Rigidbody2D>();
        myRB.AddForce(new Vector2(brickMoveSpeed, 0));
    }

    //move bricks bounce when hit the wall
    void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Wall"))
        {
            float bounce = 100f;
            myRB.AddForce(collision.contacts[0].normal * bounce);
        }

    }


}
