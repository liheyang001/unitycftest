﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;


public class GameController : MonoBehaviour
{
    [SerializeField] private Ball _ball = null;
    [SerializeField] private Transform _paddleAnchor = null;

    [SerializeField] private float _resetBallDelay = 2f;
    [SerializeField] private int _startingLives = 3;

    [SerializeField] private GameObject[] _livesCounterIcons = new GameObject[0];
    [SerializeField] private GameObject _gameOverScreen = null;
    [SerializeField] private GameObject _gamePause = null;

    public static bool gameIsPaused, playerHasKey;
    private int _lives;
    public int lives
    {
        get => _lives;
        private set
        {
            _lives = value;

            // Update lives counter
            for (int i = 0; i < _livesCounterIcons.Length; i++)
            {
                _livesCounterIcons[i].SetActive(i < lives);
            }
        }
    }

    private void Awake()
    {
        _ball.BallLostEvent += OnBallLost;
        RestartGame();
        Time.timeScale = 1;
    }

    private void OnDestroy()
    {
        _ball.BallLostEvent -= OnBallLost;
    }

    public void RestartGame()
    {
        _gameOverScreen.SetActive(false);
        _gamePause.SetActive(false);
        Cursor.visible = false;
        _ball.Initialize(_paddleAnchor);
        lives = _startingLives;
        Time.timeScale = 1;
        gameIsPaused = false;
    }

    public void ReloadGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        Time.timeScale = 1;
        _ball.Initialize(_paddleAnchor);
        lives = _startingLives;
        gameIsPaused = false;
    }
    private void Update()
    {
        // Launch ball
        if (_ball != null && _ball.state == Ball.State.Ready)
        {
            if (Input.GetMouseButtonDown(0))
            {
                _ball.Launch();
            }
        }

        //Pause game if hit "Escape"
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            gameIsPaused = !gameIsPaused;
            PauseGame();
        }

        //Go to next level if all cleaned all bricks except indestructible bricks
        if(GameObject.FindGameObjectsWithTag("Bricks_0").Length + GameObject.FindGameObjectsWithTag("Bricks_1").Length + GameObject.FindGameObjectsWithTag("Bricks_2").Length == 0)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex+1);
        }
    }

    private void OnBallLost()
    {
        if (lives > 0)
        {
            StartCoroutine(ResetBallProcess());
        }
        else
        {
            StartCoroutine(GameOverProcess());
        }
    }

    private IEnumerator ResetBallProcess()
    {
        yield return new WaitForSeconds(_resetBallDelay);

        _ball.Initialize(_paddleAnchor);
        lives--;
    }

    private IEnumerator GameOverProcess()
    {
        yield return new WaitForSeconds(_resetBallDelay);

        _gameOverScreen.SetActive(true);
        Cursor.visible = true;
    }

    public void PauseGame()
    {
        if (gameIsPaused) {
            _gamePause.SetActive(true);
            Time.timeScale = 0f;
            gameIsPaused = true;
        }
        else
        {
            _gamePause.SetActive(false);
            Time.timeScale = 1f;
            gameIsPaused = false;
        }

    }
}