﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GenerateBricks : MonoBehaviour
{
    //brick[0] = standard brick
    //brick[1] = 2 life brick
    //brick[2] = cannot destroyed brick
    public GameObject[] brick;

    public GameObject moveableBrick;
    // Start is called before the first frame update

    [SerializeField] private int columnMin, columnMax;
    [SerializeField] private int rowMin, rowMax;
    void Start()
    {
        
        //y = column, x= row
        for (int y = columnMin; y < columnMax; y++)
        {
            for (int x = rowMin; x < rowMax; x++)
            {
                int randomNumber = UnityEngine.Random.Range(0,3);
                Instantiate(brick[randomNumber], new Vector3(x, y, 0), Quaternion.identity);
            }
        }
        

        //check if every type brick has at least one
        for (int y = columnMin; y < columnMax; y++)
        {
            for (int x = rowMax; x < rowMax+1; x++)
            {
                if (GameObject.FindGameObjectWithTag("Bricks_0") == null)
                {
                    Instantiate(brick[0], new Vector3(x, y, 0), Quaternion.identity);
                }

                else if (GameObject.FindGameObjectWithTag("Bricks_1") == null)
                {
                    Instantiate(brick[1], new Vector3(x, y, 0), Quaternion.identity);
                }

                else if (GameObject.FindGameObjectWithTag("Bricks_3") == null)
                {
                    Instantiate(brick[2], new Vector3(x, y, 0), Quaternion.identity);
                }
                else
                {
                    int randomNumber = UnityEngine.Random.Range(0, 3);
                    Instantiate(brick[randomNumber], new Vector3(x, y, 0), Quaternion.identity);
                }
            }
        }
        

        //instantiate moveable brick
        for (int y = columnMax; y < columnMax+2; y++)
        {
            Instantiate(moveableBrick, new Vector3(0, y, 0), Quaternion.identity);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
